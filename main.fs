
module procedural_plant
open DawnLib.Base
open ProcPlant.Leaf
open ProcPlant.Tree
open DawnLib
open OpenTK
open System


let random_tree () =
    let i = Base.__rand.Next()
    [Trees.do_make (Some i) 99999] |> App.render_meshes <| 0 |> ignore

let random_trees () =
    let mutable i = 1
    let mutable meshes = [ProcPlant.Tree.Trees.do_make (Some(i)) 99999]
    while Input.Key.R = App.render_meshes(meshes) i do
        i <- i + 1
        meshes <- [ProcPlant.Tree.Trees.do_make (Some(i)) 99999]
        App.save_mesh (meshes.[0]) "bin/mesh.obj"

let grow_tree seed =
    let mutable i = 0
    let space:SpatialIndex = upcast new ListIndex()
    let mutable state = [TreeBud(Bud(Vector3.Zero, Vector3.UnitY, Trees.start_power, 0, None))]
    let rand = new System.Random(seed)
    let mutable key = Input.Key.Minus
    let mutable _mesh = new Mesh();
    while key <> Input.Key.Escape && Trees.buds_remain state do
        state <- Trees.do_step rand 99999 space state

        for item in state do match item with
            | Branch(br) ->
                br.Occlusion <-
                    space.Find None |> Seq.sumBy(fun box ->
                        if (box.Max - br.P2).Length > 0.5f then 0.0f
                        else 0.001f
                    )
            | TreeLeaf(t) ->
                t.Occlusion <-
                    space.Find None |> Seq.sumBy(fun box ->
                        if (box.Max - t.Pos).Length > 0.2f then 0.0f
                        else 0.001f)
            | _ -> ()

        let mesh = state |> List.collect (fun f -> f.Triangle) |> Mesh.fromTriangles
        _mesh <- mesh
        mesh.CalcNormals()
        key <- App.render_meshes [mesh] i
        i <- i + 1
    App.save_mesh _mesh "bin/mesh.obj"

[<EntryPoint>]
let main argv =
    random_tree ()
    // random_trees ()
    // grow_tree 4;
    0
