#version 330

in vec3 _nor;
in vec3 _pos;
in vec3 _bary;
in vec4 _col;
out vec4 out_color;

vec3 sun = normalize(vec3(1, 1, -1));

vec4 phong(vec4 color, vec3 normal) {
    float x = max(dot(normal, sun), 0);
    x = x * 0.5 + 0.5;
    return color * x;
}

void main() {
    out_color = phong(_col, _nor);
}
