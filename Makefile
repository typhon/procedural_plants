FILES = radiance.fs leaf.fs cone.fs tree.fs terrain.fs main.fs


run: bin/plant.exe
	mono bin/plant.exe

bin/dawnlib.dll: dawnlib/base.fs dawnlib/renderer.fs dawnlib/app.fs
	fsharpc dawnlib/base.fs dawnlib/renderer.fs dawnlib/app.fs -a -o bin/dawnlib.dll  -r packages/OpenTK/lib/net20/OpenTK.dll

bin/plant.exe: ${FILES} *.fsproj bin/dawnlib.dll
	fsharpc ${FILES} -o bin/plant.exe -r packages/OpenTK/lib/net20/OpenTK.dll -r packages/LibNoise/lib/net40/LibNoise.dll -r bin/dawnlib.dll
