module ProcPlant.Cone
open DawnLib.Base
open ProcPlant.Leaf
open OpenTK

let spiral count spirals length = seq {
    let rec loop i d angle = seq {
        let ratio = float32 i / float32 count
        let pos = Vector3(0.f, ratio * d, 0.f)
        let dir = Vector3(sin angle, 0.f, cos angle)
        yield
          Matrix4.CreateTranslation(pos) *
          Matrix4.CreateFromAxisAngle(Vector3.UnitY, angle)
        if i < count then yield! loop (i + 1) (d + length / float32 count) (angle + spirals / float32 count)
    }
    yield! loop 0 0.0f 0.0f
}

let cone = [
    let mutable i = 0;
    for m in spiral 200 200.0f 1.0f do
    i <- i + 1
    let l = Leaf.Birch
    let radius = 3.f - float32 i / 100.f
    yield! l.Triangles(Matrix4.CreateTranslation(Vector3(radius, 0.f, 0.f)) *
                       Matrix4.CreateScale(0.1f) *

                       m)
]
